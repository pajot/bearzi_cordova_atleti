/// <reference path="../dts/jquery/index.d.ts" />

import htmlString = JQuery.htmlString;

class Atleta extends Persona{
    private velocita:number;
    private div_barra:JQuery;
    private div_nome:JQuery;
    private div_metri:JQuery;
    private metri_percorsi:number=0;

    constructor(name:string,age:number,speed:number, barr:string){
        super(name,age);
        this.velocita = speed;
        this.div_barra = $(barr);
        this.div_metri = this.div_barra.find(".metri");
        this.div_nome = this.div_barra.find(".nome");
        this.initDivs();
    }
    public initDivs():void{
        this.div_nome.html(this.nome);
        this.div_metri.html(this.metri_percorsi+"mt");
        this.div_barra.css({width: "", background: ""}):
    }
    public getProfile():string{
        var out:string=super.getProfile();
        out += " che corre a "+this.velocita+" metri al secondo";
        return out;
    }
    public corri(seconds:number):void{
        var mpercorsi:number=this.velocita*seconds;
        var perdita:number=0;
        var distanza:number=0;
        while (distanza<=mpercorsi){
            perdita += (distanza/10)/100*(0.2*this.eta);
            distanza += 1 ;
        }
        this.metri_percorsi += mpercorsi - Math.round(perdita);
        this.updateBarra();
    }
    private updateBarra():void{
        if (this.metri_percorsi<500) {
            this.div_metri.html(Math.round(this.metri_percorsi)+"mt");
            this.div_barra.css({
                width: Math.round(this.metri_percorsi/500*100)+"%"
            });
        } else {
            this.div_metri.html("500mt");
            this.div_barra.css({
                width: "100%",
                background: "red"
            });
        }

    }
}
