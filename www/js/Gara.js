var Gara = (function () {
    function Gara() {
        var _this = this;
        this.div_descrizioni = $("#descrizioni");
        this.pulsante = $("#start input");
        this.pulsante.on("click", function () { return _this.clickPulsante(); });
        this.initAtleti();
        this.fillDescrizioni();
    }
    /**
     * funzione che gestisce il primo click sul pulsante start
     */
    Gara.prototype.clickPulsante = function () {
        var _this = this;
        this.reset();
        this.interval_id = setInterval(function () { return _this.updateAtleti(); }, 150);
        this.pulsante.off("click");
    };
    ;
    /**
     * funzione che inizializza gli atleti
     */
    Gara.prototype.initAtleti = function () {
        var eta = this.generaNumeroCasuale(20, 50);
        var velocita = this.generaNumeroCasuale(5, 6, 2);
        this.atleta_1 = new Atleta("Giovanni", eta, velocita, "#gara .atleta1");
        eta = this.generaNumeroCasuale(20, 50);
        velocita = this.generaNumeroCasuale(5, 6, 2);
        this.atleta_2 = new Atleta("Andrea", eta, velocita, "#gara .atleta2");
        eta = this.generaNumeroCasuale(20, 50);
        velocita = this.generaNumeroCasuale(5, 6, 2);
        this.atleta_3 = new Atleta("Anna", eta, velocita, "#gara .atleta3");
        this.atleti = [this.atleta_1, this.atleta_2, this.atleta_3];
    };
    ;
    /**
     * utility per generare numeri casuali
     * porting dalla funzione creata in precedenza per gli esercizi
     * @param minimo
     * @param massimo
     * @param decimali
     * @returns {number}
     */
    Gara.prototype.generaNumeroCasuale = function (minimo, massimo, decimali) {
        if (decimali === void 0) { decimali = 0; }
        var fattore_decimali = Math.pow(10, decimali);
        var diff = (massimo * fattore_decimali) - (minimo * fattore_decimali);
        var numero = Math.random();
        var var_casuale = Math.round(numero * diff);
        var out = (((minimo * fattore_decimali) + var_casuale) / fattore_decimali);
        return out;
    };
    ;
    /**
     * funzione che aggiorna gli atleti ad intervalli regolari
     */
    Gara.prototype.updateAtleti = function () {
        var _this = this;
        var finito = false;
        for (var i = 0; i < this.atleti.length; i++) {
            this.atleti[i].corri(1);
            if (this.atleti[i].metri_percorsi >= 500) {
                finito = true;
            }
        }
        if (finito) {
            clearInterval(this.interval_id);
            this.pulsante.on("click", function () { return _this.clickPulsante(); });
        }
    };
    ;
    /**
     * funzione che ripristina lo stato iniziale
     */
    Gara.prototype.reset = function () {
        this.initAtleti();
        this.fillDescrizioni();
    };
    ;
    /**
     * funzione che aggiorna le descrizioni degli atleti
     */
    Gara.prototype.fillDescrizioni = function () {
        for (var i = 0; i < this.atleti.length; i++) {
            var atleta = this.atleti[i];
            var div = this.div_descrizioni.find(".atleta" + (i + 1));
            div.html(atleta.getProfile());
        }
    };
    return Gara;
}());
var gara = new Gara();
//# sourceMappingURL=Gara.js.map