class Gara{
    private pulsante:JQuery;
    private atleta_1;
    private atleta_2;
    private atleta_3;
    private atleti;
    private div_descrizioni:JQuery;
    private interval_id:number;
    constructor(){
        this.div_descrizioni = $("#descrizioni");
        this.pulsante = $("#start input");
        this.pulsante.on("click",() => this.clickPulsante());
        this.initAtleti();
        this.fillDescrizioni();
    }
    /**
     * funzione che gestisce il primo click sul pulsante start
     */
    private clickPulsante():void{
        this.reset();
        this.interval_id = setInterval(()=>this.updateAtleti(),150);
        this.pulsante.off("click");
    };
    /**
     * funzione che inizializza gli atleti
     */
    private initAtleti():void{
        var eta:number = this.generaNumeroCasuale(20,50);
        var velocita:number = this.generaNumeroCasuale(5,6,2);
        this.atleta_1 = new Atleta("Giovanni",eta,velocita,"#gara .atleta1");
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        this.atleta_2 = new Atleta("Andrea",eta,velocita,"#gara .atleta2");
        eta = this.generaNumeroCasuale(20,50);
        velocita = this.generaNumeroCasuale(5,6,2);
        this.atleta_3 = new Atleta("Anna",eta,velocita,"#gara .atleta3");
        this.atleti = [this.atleta_1,this.atleta_2,this.atleta_3];
    };
    /**
     * utility per generare numeri casuali
     * porting dalla funzione creata in precedenza per gli esercizi
     * @param minimo
     * @param massimo
     * @param decimali
     * @returns {number}
     */
    private generaNumeroCasuale(minimo:number, massimo:number, decimali:number=0):number{
        var fattore_decimali:number = Math.pow(10,decimali);
        var diff:number = (massimo*fattore_decimali)-(minimo*fattore_decimali);
        var numero:number = Math.random();
        var var_casuale:number = Math.round(numero*diff);
        var out:number = (((minimo*fattore_decimali) + var_casuale)/fattore_decimali);
        return out;
    };
    /**
     * funzione che aggiorna gli atleti ad intervalli regolari
     */
    private updateAtleti():void{
        var finito:boolean = false;
        for (var i:number=0; i<this.atleti.length; i++) {
            this.atleti[i].corri(1);
            if(this.atleti[i].metri_percorsi>=500){
                finito = true;
            }
        }
        if(finito){
            clearInterval(this.interval_id);
            this.pulsante.on("click",()=>this.clickPulsante());
        }
    };
    /**
     * funzione che ripristina lo stato iniziale
     */
    private reset():void{
        this.initAtleti();
        this.fillDescrizioni();

    };

    /**
     * funzione che aggiorna le descrizioni degli atleti
     */
    private fillDescrizioni():void{
        for (var i:number=0; i<this.atleti.length; i++){
            var atleta = this.atleti[i];
            var div = this.div_descrizioni.find(".atleta" + (i + 1));
            div.html(atleta.getProfile());
        }
    }
}
 var gara=new Gara();
